create schema six2;

create table six2.department(id int primary key,name varchar(20),location varchar(20));
drop table six2.department;

create table six2.employee(id int primary key,name varchar(20),job_name varchar(20),manager_id int,hier_date date,
salary int,commission int,department_id int,foreign key(department_id) references department(id));
drop table six2.employee;

insert into six2.department values (2001,'customer_services','first_block'),
(2002,'management','second_block'),
(2003,'marketing','third_block'),
(2004,'production','fourth_block'),
(2005,'sales','fifth_block');
select * from six2.department;
truncate six2.employee;

insert into six2.employee values (1,'priya','manager',101,"2021-03-13",80005,40000,2002),
(2,'vidya','worker',101,"2021-02-14",30000,50000,2001),
(3,'bava','asst_manager',102,"2021-03-17",50000,40000,2004),
(4,'pavai','asst_manager',102,"2021-03-12",50000,40000,2005),
(5,'shalini','worker',101,"2021-02-02",20000,30000,2005),
(6,'jibril','manager',101,"2021-03-08",80005,40000,2002),
(7,'krishna','worker',102,"2021-01-20",40000,40000,2003),
(8,'kalidass','manager',102,"2021-03-13",80007,30000,2002),
(9,'bharath','worker',101,"2021-01-24",30009,40000,2005),
(10,'varsha','asst_manager',101,"2021-01-07",50000,40000,2004);
select * from six2.employee;

select salary,name from six2.employee;
select distinct job_name,d.name
 from six2.employee e inner join six2.department d on e.department_id=d.id;
select id,salary,commission from six2.employee;
select e.name from six2.employee e inner join six2.department d on e.department_id=d.id 
where d.id != 2001;
select name from six2.employee where salary < commission;
select name from six2.employee where length(name)=6;
select name from six2.employee where salary%2=1;