create schema sch;
use sch;
create table detail(id int,name varchar(20),age varchar(10),last_name varchar(20));
insert into detail values(1,'priya','20','dharshini'),(2,'vidya','14','nandhini'),
(3,'bava','17','dharani');
select*from detail;
alter table detail change column name first_name varchar(20);
alter table detail add column last_name varchar(20);
alter table detail modify column age int;
drop table detail;
delete from detail where id='3';

