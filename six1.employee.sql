create schema six1;

create table six1.dept(id int primary key,name varchar(20),location varchar(20));

create table six1.employee(id int,name varchar(10),salary int,joining_date date,
dept_id int, foreign key(dept_id) references six1.dept(id));

insert into six1.dept values (101,'customer_services','first_block'),
(102,'management','second_block'),
(103,'marketing','third_block'),
(104,'production','fourth_block'),
(105,'sales','fifth_block');
select * from six1.dept;

insert into six1.employee values (2001,'vidya',25000,"2021-02-14",101),
(2002,'kalidass',80000,"2021-03-13",102),
(2003,'priya',85000,"2021-03-13",102),
(2004,'jibril',65000,"2021-06-08",103),
(2005,'bharath',50000,"2021-07-24",104),
(2006,'krishna',45000,"2021-10-20",105),
(2007,'bava',30000,"2021-10-17",101),
(2008,'shalini',60000,"2021-10-02",103),
(2009,'pavai',30000,"2021-07-12",105),
(2010,'sanu',75000,"2021-05-11",102);
select * from six1.employee;
drop table six1.employee;

select e.name from six1.employee e inner join six1.dept d on e.dept_id=d.id where d.name='marketing';
select * from six1.employee where joining_month='august';
select * from six1.employee where salary=(select max(salary) from six1.employee);
select count(e.name),d.name from six1.employee e inner join six1.dept d on e.dept_id=d.id 
group by d.name;
select * from six1.employee where joining_date between "2021-08-31" and "2021-11-01";