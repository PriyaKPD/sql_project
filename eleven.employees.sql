create schema eleven;
create table eleven.employees(id int primary key,fname varchar(20),lname varchar(20),age int,salary int);
 insert into eleven.employees values (101,'priya','kumar',21,75000),
(102,'vidya','karthik',1001,70000),
(103,'riya','baskar',28,55000),
(104,'sriya','karthik',32,33000),
(105,'bava','ajay',36,45000),
(106,'sara','naresh',38,10000),
(107,'monica','dell',40,30000),
(108,'mona','cibi',14,15000),
(109,'keerthana','ravi',43,46000),
(110,'monisha','kennedy',21,24000);
select * from eleven.employees;
drop table eleven.employees;
truncate table eleven.employees;

create table eleven.mobile(id int primary key,number bigint,
operator varchar(10),plan varchar(20),employee_id int,foreign key(employee_id) references employees(id));

insert into eleven.mobile values (1001,9345071753,'jio','2gb/day',101),
 (1002,9346071723,'vi','1gb/day',102),
 (1003,9345078753,'airtel','3gb/day',103),
 (1004,9345077753,'jio','6gb/day',104),
 (1005,9845271753,'vi','8gb/day',105),
 (1006,9945071753,'jio','2gb/day',106),
 (1007,9443023300,'airtel','2gb/day',107),
(1008,9443023300,'vi','2.5gb/day',108),
(1009,9333025500,'jio','3gb/day',109),
(1010,9783023300,'jio','1gb/day',110),
(1011,9443023300,'airtel','5gb/day',101),
(1012,9333567300,'vi','9gb/day',105),
(1013,9339803300,'bsnl','1gb/day',106),
(1014,9339813300,'vi','2gb/day',107),
(1015,9339823300,'jio','3gb/day',101),
(1016,9339833300,'airtel','4gb/day',102),
(1017,9339843300,'bsnl','5gb/day',109),
(1018,9339853300,'vi','2gb/day',110);
select * from eleven.mobile;
drop table eleven.mobile;
create table eleven.email(id int primary key,email varchar(20),provider varchar(20),
created_date date,employee_id int,foreign key(employee_id) references employees(id));

insert into eleven.email values (1,'pri@gmail.com','gmail',"2012-12-02",101),
(2,'sri@yahoo.com','yahoo',"2019-07-02",102),
(3,'kri@gmail.com','gmail',"2014-04-12",103),
(4,'kli@yahoo.com','yahoo',"2020-09-01",104),
(5,'bri@gmail.com','gmail',"2011-03-02",105),
(6,'bava@gmail.com','gmail',"2018-01-18",106),
(7,'vid@gmail.com','gmail',"2016-05-28",107),
(8,'vicky@yahoo.com','yahoo',"2017-07-08",108),
(9,'vicky@yahoo.com','gmail',"2010-09-08",109),
(10,'vicky@yahoo.com','gmail',"2011-10-22",110),
(11,'efd@yahoo.com','yahoo',"2013-12-11",101),
(12,'karthi19@gmail.com','gmail',"2012-02-19",103),
(13,'karthi19@gmail.com','yahoo',"2014-04-24",105),
(14,'edi@gmail.com','gmail',"2015-09-07",107),
(15,'pebi@gmail.com','gmail',"2017-12-28",109);
select * from eleven.email;
drop table eleven.email;

select employees.fname,count(distinct mobile.number),count(distinct email.email) from eleven.employees 
inner join eleven.mobile on employees.id=mobile.employee_id
 right outer join eleven.email on employees.id=email.employee_id group by fname;
 
 select employees.fname,mobile.number from eleven.employees left outer join eleven.mobile on employees.id=mobile.employee_id 
 group by number;
 
 select email.email,count(email) from eleven.employees left outer join eleven.email on employees.id=email.employee_id 
 group by email having count(email)>1;



select distinct j1.fname,j1.mobile_count,j2.email_count from 
(select employees.id,employees.fname,count(mobile.number) as 
mobile_count from eleven.employees left outer join eleven.mobile on mobile.employee_id=employees.id group by employees.id) as j1
inner join 
(select employees.id,employees.fname,count(email) as 
email_count from eleven.employees left outer join eleven.email on email.employee_id=employees.id group by employees.id) as j2
 on j1.id=j2.id;