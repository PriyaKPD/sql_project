create schema six;

create table six.dept(id int primary key,name varchar(20),location varchar(20));

create table six.employee(id int,name varchar(10),salary int,joining_month varchar(10),
dept_id int, foreign key(dept_id) references six.dept(id));

insert into six.dept values (101,'customer_services','first_block'),
(102,'management','second_block'),
(103,'marketing','third_block'),
(104,'production','fourth_block'),
(105,'sales','fifth_block');
select * from six.dept;

insert into six.employee values (2001,'vidya',25000,'july',101),
(2002,'kalidass',80000,'august',102),
(2003,'priya',85000,'august',102),
(2004,'jibril',65000,'june',103),
(2005,'bharath',50000,'july',104),
(2006,'krishna',45000,'august',105),
(2007,'bava',30000,'october',101),
(2008,'shalini',60000,'october',103),
(2009,'pavai',30000,'july',105),
(2010,'sanu',75000,'august',102);
select * from six.employee;

select e.name from six.employee e inner join six.dept d on e.dept_id=d.id where d.name='marketing';
select * from six.employee where joining_month='august';
select * from six.employee where salary=(select max(salary) from six.employee);
select count(e.name),d.name from six.employee e inner join six.dept d on e.dept_id=d.id 
group by d.name;
