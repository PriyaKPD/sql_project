create database big;
use big;
CREATE TABLE employee(id int PRIMARY KEY,name varchar(20),age int,email varchar(20),fname varchar(20),
lname varchar(20),doorno int,street varchar(20),area varchar(20),
city VARCHAR(10),district VARCHAR(20),
state VARCHAR(15),pincode int,country VARCHAR(20),school_id int,school_name VARCHAR(20),
school_code int,school_telno DOUBLE,v_doorno int,school_street VARCHAR(20),school_area varchar(20),
school_city VARCHAR(20),school_district VARCHAR(20),school_state VARCHAR(20),school_pincode int,
school_owner_name VARCHAR(20),school_mobNo double,college_id VARCHAR(5),college_code varchar(20),
college_telNo double,college_doorNo int,college_street varchar(20),college_area varchar(15),
college_city varchar(15), college_district VARCHAR(15),college_state varchar(15),college_pincode int,
college_ownerName VARCHAR(20),college_mobNo double);
select*from employee;
insert into employee values
(1,"shiva",20,"shiva@gmail.com","shiva","kumar",233,"west","templetown","kumbakonam","thanjavur","tamilnadu",614203,"india",101,"akms","101",0437422805,237,"west street",
"mainroad","kumbakonam","thanjavur","tamilnadu",614305,"joseph",79909076545,"as101","aaaaa",04374222222,
341,"west street","mainroad","kumbakonam","thanjavur","tamilnadu",612234,"raaja",9003782352);
insert into employee values
(2,"sakthi",23,"sakthi@gamil.com","sakthi","sakthi",231,"east","market","chennai","chennai","tamilnadu",614122,"india",102,"amms",122,0437424305,456,"south street","templeroad",
"chennai","chennai","tamilnadu",614905,"steev",79559076545,"as101","aaaaa",04374222222,341,"west street","mainroad",
"chennai","chennai","tamilnadu",612434,"emrey",9223790352);

insert into employee values
(3,"ram",22,"ram@gmail.com","ram","krishna",421,"south","templetown","salem","salem","tamilnadu",618903,"india",103,"bmms",555,0445422805,137,"west street","crossstreet",
"kumbakonam","thanjavur","tamilnadu",614305,"ranjith",63379076545,"as101","aaaaa",04374222222,341,"west street","mainroad",
"kumbakonam","thanjavur","tamilnadu",612234,"raaja",9003782352);

insert into employee values
(4,"reena",20,"reena@gmail.com","reena","reena",321,"east","mainroad","erode","erode","tamilnadu",611203,"india",102,"amms",333,0434422805,456,"south street","templeroad",
"chennai","chennai","tamilnadu",614905,"steev",79909076545,"as102","bbbbb",04374222222,341,"west street","mainroad",
"trichy","trichy","tamilnadu",619934,"ranjan",7890552355);
insert into employee values
(5,"priya",21,"priya@gmail.com","priya","sri",532,"north","main","madurai","madurai","tamilnadu",618903,"india",101,"akms",333,0437422805,237,"west street","mainroad",
"kumbakonam","thanjavur","tamilnadu",614305,"joseph",79909076545,"as102","bbbbb",04374782222,377,"east street","churchroad",
"erode","erode","tamilnadu",612834,"maharajan",8248123452);
insert into employee values
(6,"vikram",22,"vikram@gmail.com","vikram","ram",324, "east","market","kumbakonam","thanjavur","tamilnadu",614255,"india",104,"amms","222",0437422805,456,"west street","mainroad",
"salem","salem","tamilnadu",614775,"vinothkumar",7355512345,"as101","aaaaa",04374222222,341,"west street","mainroad",
"kumbakonam","thanjavur","tamilnadu",612234,"raaja",9003711352);

insert into employee values
(7,"jeeva",22,"jeeva@gmail.com","jeeva","jeeva",377,"east","mainroad","salem","salem","tamilnadu",610003,"india",101,"akms","777",0437422805,237,"west street","mainroad",
"madurai","madurai","tamilnadu",611305,"jragav",8132456780,"bs101","ccccc",04374222222,131,"north street","churchroad",
"kumbakonam","thanjavur","tamilnadu",612234,"raaja",9234523544);
insert into employee values
(8,"sruthi",23,"sruthi@gmail.com","sruthi","sruthi",301,"west","market","coimbatore","coimbatore","tamilnadu",614603,"india",105,"bmms","555",0455422805,237,"east street","mainroad",
"tuthukudi","tuthukudi","tamilnadu",614305,"krishan",82219076545,"bs102","ddddd",04374277222,311,"north street","bazaarroad",
"kumbakonam","thanjavur","tamilnadu",612234,"raaja",7890634565);
insert into employee values
(9,"frankleng",20,"frank@gmail.com","frank","leng",422,"north","west","pettai","tirunelveli","tamilnadu",618803,"india",107,"jjms","311",0437489805,127,"south street","crossroad",
"kumbakonam","thanjavur","tamilnadu",614355,"ranjith",7134576545,"cs102","ccccc",04374222555,231,"south road","southroad",
"krishnagiri","krishnagiri","tamilnadu",612244,"raajaram",90233782352);
insert into employee values
(10,"sita",22,"sita@gamil.com","sita","ram",555,"west","churchroad","dharmapuri","dharmapuri","tamilnadu",619003,"india",109,"abms","444",0437422905,287,"west street","main road",
"erode","erode","tamilnadu",617305,"krishnan",7333476545,"cs101","abaaa",04374244222,241,"east street","templeroad",
"madurai","madurai","tamilnadu",612234,"vignesh",9037256575);
select*from employee;
select name,district from employee group by district;
select name,age from employee order by age desc;
select*from employee where age=(select max(age) from employee);