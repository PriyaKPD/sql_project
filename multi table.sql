create schema multi;

create table multi.auditor(id int primary key,name varchar(10),age int,email varchar(20));

insert into multi.auditor values (1001,'priya',23,'pri2@gmailcom');
insert into multi.auditor values (1002,'vidya',22,'vidk@gmailcom');
insert into multi.auditor values (1003,'miya',22,'miya3@gmailcom');
insert into multi.auditor values (1004,'bava',24,'bavas@gmailcom');
insert into multi.auditor values (1005,'swetha',25,'swem@gmailcom');
select * from multi.auditor;

create table multi.college(id int primary key,name varchar(20),year int,area varchar(20));

insert into multi.college values (101,'pits',2007,'thanjavur');
insert into multi.college values (102,'vit',2005,'vellore');
insert into multi.college values (103,'nit',2009,'chennai');
insert into multi.college values (104,'fx',2006,'thirunelveli');
select * from multi.college;

create table multi.work(id int,auditor_id int,college_id int,
foreign key(auditor_id) references multi.auditor(id),foreign key(college_id) references multi.college(id));
drop table multi.work;
insert into multi.work values (1,1001,101);
insert into multi.work values (2,1002,101);
insert into multi.work values (3,1002,102);
insert into multi.work values (4,1001,103);
insert into multi.work values (5,1001,104);
insert into multi.work values (6,1003,101);
insert into multi.work values (7,1004,105);
insert into multi.work values (8,1005,104);
insert into multi.work values (9,1005,103);
insert into multi.work values (10,1004,102);
select* from multi.work;
desc multi.work;
select clg.name,au.name from multi.work wr inner join multi.college clg 
		inner join multi.auditor au on wr.college_id=clg.id and au.id=wr.auditor_id where clg.id=101; 
select clg.name,au.name from multi.work wr inner join multi.college clg 
		inner join multi.auditor au on wr.college_id=clg.id and au.id=wr.auditor_id order by clg.name,au.name; 
select clg.name,au.name from multi.work wr inner join multi.college clg 
		inner join multi.auditor au on wr.college_id=clg.id and au.id=wr.auditor_id order by clg.name;
