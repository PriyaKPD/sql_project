create schema sm;

create table sm.student_marks(id int primary key,name varchar(10),subject varchar(10),semester int,marks int);

insert into sm.student_marks values (1,'tiger','english',1,70),(2,'tiger','physics',1,80),
(3,'tiger','chemistry',1,90),(4,'lion','english',1,65),
(5,'lion','physics',1,55),(6,'lion','chemistry',1,70),
(7,'zebra','english',1,79),(8,'zebra','physics',1,81),
(9,'zebra','chemistry',1,76),(10,'fox','english',1,82),
(11,'fox','physics',1,89),(12,'fox','chemistry',1,95);
select * from sm.student_marks;
select subject,name,max(marks) from sm.student_marks group by subject;
select name,marks from sm.student_marks;

select name from sm.student_marks where marks=(select max(marks) from sm.student_marks
 where subject="english" group by subject) and subject='english';

select name from sm.student_marks where marks=(select max(marks) from sm.student_marks
 where subject="english" group by subject) and subject='english';
 
 select * from (select name, avg(marks) mrk from sm.student_marks group by name) av where av.mrk = 80.0000;
 
 select max(marks) from sm.student_marks
 where subject='english' group by subject;
 select max(marks) from sm.student_marks group by subject;
 
 select name, mrk from (select name,avg(marks) mrk from sm.student_marks group by name) t1 where 
t1.mrk = (select max(mrk) from (select name, avg(marks) mrk from sm.student_marks group by name) avrg);
