create schema ten;

create table ten.employees(id int primary key,fname varchar(20),lname varchar(20),mobile_no bigint,
email varchar(25),age int,salary int);
drop table ten.employees;
insert into ten.employees values (101,'priya','kumar',9345071753,'pri@gmail.com',21,75000),
(102,'vidya','karthik',9345044753,'vid@gmail.com',15,70000),
(103,'priya','baskar',9345071753,'prib@gmail.com',28,55000),
(104,'priya','karthik',9345371753,'bavas@gmail.com',32,33000),
(105,'bava','ajay',9845071753,'bavajay@gmail.com',36,45000),
(106,'vidya','naresh',9342071753,'vn@gmail.com',38,10000),
(107,'monica','dell',9345071713,'imdc@gmail.com',40,30000),
(108,'monica','cibi',9345971753,'mcaa@gmail.com',14,15000),
(109,'keerthana','ravi',9365071753,'keeravi@gmail.com',43,46000),
(110,'monisha','kennedy',9345091753,'monikennedy@gmail.com',21,24000);
select * from ten.employees;
select fname from ten.employees 
where salary>20000 and age between 20 and 45 
group by fname having count(fname)>2 order by fname desc;
