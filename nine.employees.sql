create schema nine;

create table nine.employees(id int primary key,name varchar(15),age int,email varchar(25),
company_name varchar(15),salary int,height int,employee_location varchar(20),cmpny_id int,foreign key(cmpny_id) references company(id));
drop table nine.employees;
insert into nine.employees values (1,"shiva",27,"shiva@gmail.com","company1",15000,158,'thirunelveli',101),
(2,"sree",32,"sree@gmail.com","company1",16000,166,'coimbatore',103),
(3,"kavi",40,"kavi@gmail.com","company1",17000,160,'chennai',105),
(4,"shiva",35,"shiva01@gmail.com","company1",18000,160,'banglore',107),
(5,"seeta",35,"seeta@gmail.com","company3",19000,163,'chennai',109),
(6,"ram",26,"ram@gmail.com","company2",20000,148,'kanyakumari',101),
(7,"sakthi",27,"shiva@gmail.com","company3",21000,152,'banglore',103),
(8,"sita",28,"sita@gmail.com","company2",22000,157,'krishnagiri',105),
(9,"ram",38,"ram05@gmail.com","comapny3",24000,159,'coimbatore',107),
(10,"priya",25,"priya@gmail.com","company2",25000,162,'chennai',109);
select*from nine.employees;
select * from nine.employees where age<25 or height>160 and salary>20000;
select name,age,salary from nine.employees where age between 25 and 35 and salary>20000;
select name,age,salary from nine.employees where age between 25 and 35 and age<>28 and salary=15000 or salary=17000;
select name from nine.employees where company_name='company1' or company_name='company2';
select name from nine.employees where company_name!='company1' and company_name!='company2';
alter table nine.employees add column (fname varchar(20),lname varchar(20));
select * from nine.employees;
update nine.employees set fname='arun',lname='jacob' where id=1;
update nine.employees set fname='mano',lname='nandhan' where id=2;
update nine.employees set fname='kumar' ,lname='vasudev'where id=3;
update nine.employees set fname='krishnan',lname='madhusudhanan' where id=4;
update nine.employees set fname='cleef',lname='mani' where id=5;
update nine.employees set fname='nilson',lname='lias' where id=6;
update nine.employees set fname='demon' ,lname='ram'where id=7;
update nine.employees set fname='sivan',lname='jacob'where id=8;
update nine.employees set fname='ram',lname='radha' where id=9;
update nine.employees set fname='priya',lname='karthick' where id=10;
select*from nine.employees;
select * from nine.employees where fname like 'a%' and lname like '%b';
update nine.employees set name='adid' where id=4;
select * from nine.employees where name like 'a__d%';
select sum(salary) from nine.employees;
select avg(salary) from nine.employees;
select count(name) from nine.employees;
alter table nine.employees add column department varchar(20);

update nine.employees set department='customer_service' where id in (1,2,5);
update nine.employees set department='production' where id in (6,7,4,8);
update nine.employees set department='management' where id in (3,9,10);
select * from nine.employees;
select name,salary,department from nine.employees where salary in (select max(salary) from nine.employees group by department); 
select name,salary,department from nine.employees where salary in (select min(salary) from nine.employees group by department); 
select name,salary,department from nine.employees where salary in (select max(salary) from nine.employees where id between 3 and 7) ; 
select count(name),company_name from nine.employees where company_name='company1';
select count(name) from nine.employees where salary>=15000;
select name,id,salary from nine.employees where salary>=15000 order by id desc;
select name,id,salary from nine.employees where salary>=15000 order by id,name desc;
select name,id,salary from nine.employees where salary>=15000 order by id;
select * from nine.employees order by salary;
select sysdate() from nine.employees;

 create table nine.company(id int primary key,name varchar(20),
 location varchar(20),city varchar(20)); 
 
 insert into nine.company values (101,'cmp1',',manglore','banglore'),
 (103,'cmp2',',ecr','chennai'),
 (105,'cmp3','gandhipuram','coimbatore'),
 (107,'cmp4','uthangarai','krishnagiri'),
 (109,'cmp5','chathram','trichy');
 select * from nine.company;
 select company.name,employees.name from nine.company inner join nine.employees 
 on company.id=employees.cmpny_id where 'company.city'='employees.location';
 select company.name,employees.name from nine.company inner join nine.employees 
 on company.id=employees.cmpny_id where 'company.city'!='employees.location';
 select company.name,employees.name,salary from nine.company inner join nine.employees 
 on company.id=employees.cmpny_id where 'company.city'!='employees.location' and salary>10000;
 select company.name,count(employees.name) from nine.company inner join nine.employees 
 on company.id=employees.cmpny_id where company.name in (select name from nine.company where id=105);
 select avg(salary) from nine.employees where cmpny_id = 101;
 select count(employees.name) from nine.company inner join nine.employees 
 on company.id=employees.cmpny_id where salary>(select avg(salary) from nine.employees where cmpny_id = 101);
 select id,name from nine.employees where  employee_location='banglore'
  union all  
 select id,name from nine.company where city='banglore';
 
 select id,employee_location,salary from nine.employees
 union all
 select id,location,city from nine.company;
  

 select employees.name,salary high_salary from nine.employees where salary>17000;
 select employees.name,salary low_salary from nine.employees where salary<=17000;
 
 select id,fname,lname,salary,if(salary>17000,'high salary','low salary')salary_type 
 from nine.employees order by salary_type;
 
 select distinct(fname) from nine.employees;
 alter table nine.employees add column expenses int;
 
 update nine.employees set expenses=5000 where id in (1,3,7,9);
 update nine.employees set expenses=7000 where id in (4,6,8,10);
 select * from nine.employees;
 select * from nine.employees where expenses is null;
 
 create table nine.sales(id int primary key,custome_name varchar(20),order_date date);
 
 insert into nine.sales values (2001,'priya',"2020-01-11"),(2002,'ram',"2022-02-22"),(2003,'sanu',"2020-10-20"),
 (2004,'raj',"2021-03-12"),
 (2005,'karthick',"2020-08-07"),
 (2006,'keerthi',"2021-02-28");
 select * from nine.sales;
 select * from nine.sales order by custome_name,order_date;

select max(age),min(age) from nine.employees;
select * from nine.employees order by id desc limit 5;
select name,count(fname) from nine.employees group by fname;
select * from nine.employees order by name desc limit 3;
select upper(fname) as empname from nine.employees;
select * from nine.employees;
select name,salary from nine.employees where salary between 50000 and 100000;
select name from nine.employees where name like 's%';
select *,concat(fname,' ',lname)as fullname from nine.employees;
select fname from nine.employees where fname like '_a%';

alter table nine.employees add column joined_year year;

update nine.employees set joined_year=2020 where id in (1,4,10);
update nine.employees set joined_year=2021 where id in (2,5,7,8);
update nine.employees set joined_year=2022 where id in (3,6,9);
select name,joined_year from nine.employees where joined_year=2020;

create table nine.employees1(select * from nine.employees);
select name from nine.employees1 where id between 2 and 7;
select salary,age,name from nine.employees1 where salary=(select max(salary) from nine.employees1) 
and age=(select min(age) from nine.employees1);
SELECT * FROM nine.employees1 ORDER BY salary DESC LIMIT 1 OFFSET 2;

alter table nine.employees1 add column department varchar(20);

update nine.employees1 set department='csc' where id in (1,2,10);
update nine.employees1 set department='' where id in (1,2,10);
update nine.employees1 set department=csc where id in (1,2,10);
select * from nine.employees1;
select max(salary),name from nine.employees1 group by department;
select * from nine.employees1 order by salary desc limit 7;
