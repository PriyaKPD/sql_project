create schema basic;
use basic;
create table students(student_id int,name varchar(20),age int,course varchar(10),
year_of_study varchar(20));
insert into students values('s1','priya','21','physics','second_year');
insert into students values ('s2','vidya','22','history','third_year');
insert into students values ('s3','bava','23','computer','third_year');
insert into students values ('s4','varsha','24','physics','first_year');
insert into students values ('s5','jeeva','25','tamil','second_year');
select*from students;
alter table students modify age varchar(10);
