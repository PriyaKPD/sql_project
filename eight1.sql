create schema eight1;
create table eight1.employee(id int primary key,name varchar(20),age int,email varchar(20));
insert into eight1.employee(id,name,age,email) values 
(101,'pri',22,'pri1@gmail.com');
insert into eight1.employee(id,name,age,email) values 
(102,'kli',23,'kli1@gmail.com');
insert into eight1.employee(id,name,age,email) values 
(103,'kri',22,'kri1@gmail.com');
insert into eight1.employee(id,name,age,email) values 
(104,'bri',21,'bri1@gmail.com');
insert into eight1.employee(id,name,age,email) values 
(105,'sri',23,'sri1@gmail.com');
select*from eight1.employee;

create table eight1.mobile(id int,number bigint,operator varchar(10),
package varchar(10),employee_id int );
insert into eight1.mobile values(11, 78798982939,"airtel","1.5gb/day",101);
insert into eight1.mobile values(12, 7879800939,"vodafone","2gb/day",102);
insert into eight1.mobile values(13, 7879898539,"airtel","1.5gb/day",103);
insert into eight1.mobile values(14, 7879898269,"jio","1.5gb/day",101);
insert into eight1.mobile values(15, 7989829390,"airtel","1.5gb/day",101);
insert into eight1.mobile values(16, 7822802939,"jio","2gb/day",102);
insert into eight1.mobile values(17, 78798982939,"vi","1gb/day",103);
insert into eight1.mobile values(18, 7875552939,"airtel","1gb/day",101);
insert into eight1.mobile values(19, 7876682939,"jio","2gb/day",104);
select*from eight1.mobile;
use eight1;
alter table mobile add constraint foreign key(employee_id) references employee(id);
drop table eight1.mobile;
drop table eight1.employee;
select name,count(name) from eight1.employee e left outer join eight1.mobile m on e.id=m.employee_id 
group by e.name;
describe eight1.mobile;
desc eight1.employee;