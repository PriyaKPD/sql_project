create schema seven2;

create table seven2.student(id int primary key, name varchar(20),age int);
insert into seven2.student(id,name,age)values(1,'keerthi',20);
insert into seven2.student(id,name,age)values(2,'riya',18);
insert into seven2.student(id,name,age)values(3,'preethi',19);
insert into seven2.student(id,name,age)values(4,'sri',17);
insert into seven2.student(id,name,age)values(5,'selvi',16);
insert into seven2.student(id,name,age)values(6,'agalya',17);
insert into seven2.student(id,name,age)values(7,'kaviya',18);
insert into seven2.student(id,name,age)values(8,'nivi',15);
insert into seven2.student(id,name,age)values(9,'saranya',17);
insert into seven2.student(id,name,age)values(10,'priya',17);
select*from seven2.student ;
drop table student;

select count(name) minor from seven2.student where age<18;
select count(name) major from seven2.student where age>=18;

alter table seven2.student add column(english int,tamil int,maths int,physics int,chemistry int);

update seven2.student set tamil=90,english=90,maths=96,physics=98,chemistry=99 where id=1;
update seven2.student set tamil=74,english=80,maths=97,physics=95,chemistry=89 where id=2;
update seven2.student set tamil=81,english=88,maths=99,physics=32,chemistry=90 where id=3;
update seven2.student set tamil=93,english=99,maths=99,physics=92,chemistry=90 where id=4;
update seven2.student set tamil=94,english=89,maths=94,physics=93,chemistry=83 where id=5;
update seven2.student set tamil=78,english=96,maths=95,physics=89,chemistry=93 where id=6;
update seven2.student set tamil=89,english=99,maths=99,physics=28,chemistry=69 where id=7;
update seven2.student set tamil=97,english=91,maths=99,physics=78,chemistry=88 where id=8;
update seven2.student set tamil=88,english=92,maths=89,physics=81,chemistry=78 where id=9;
update seven2.student set tamil=78,english=82,maths=95,physics=80,chemistry=92 where id=10;
select * from seven2.student;
drop table seven2.student;

select max(english),name from seven2.student where english=(select max(english) from seven2.student);
select max(tamil),name from seven2.student where tamil=(select max(tamil) from seven2.student);
select max(maths),name from seven2.student where maths=(select max(maths) from seven2.student);
select max(physics),name from seven2.student where physics=(select max(physics) from seven2.student);
select max(chemistry),name from seven2.student where chemistry=(select max(chemistry) from seven2.student);

select sum(english+tamil+maths+physics+chemistry),name from seven2.student group by name;
select max(total.s) from (select sum(english+tamil+maths+physics+chemistry)as s from seven2.student group by name) as total;
select min(total.s) from (select sum(english+tamil+maths+physics+chemistry)as s from seven2.student group by name) as total;
alter table seven2.student add column(std int,section varchar(5));

update seven2.student set std=12 where id between 1 and 10;
update seven2.student set section='a' where id in (1,2,5);
update seven2.student set section='b' where id in (3,4);
update seven2.student set section='c' where id in (6,8,10);
update seven2.student set section='a' where id in (7,9);
select physics,name from seven2.student where physics>=35;

