create schema stu;
use stu;
CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `marks` int(11) DEFAULT NULL,
  `ranks` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

insert into students (id, name, age, email, marks, ranks) values 
(1,'Raju kumar',35,'raju@gmail.com',90,2);
insert into students (id, name, age, email, marks, ranks) values 
(2,'Ramesh Kumar',40,'ramesh@g.com',81,3);
insert into students (id, name, age, email, marks, ranks) values 
(3,'Suresh Babu',42,'suresh@g.com',72,4);
insert into students (id, name, age, email, marks, ranks) values 
(4,'Balaji',70,'bala@g.com',65,6);
insert into students (id, name, age, email, marks, ranks) values 
(5,'Kajith Raju',63,'kaj@g.com',40,7);
insert into students (id, name, age, email, marks, ranks) values 
(6,'Gowtham Kumar',47,'gow@g.com',69,5);
insert into students (id, name, age, email, marks, ranks) values 
(7,'Mohan Kumar',40,'moh@g.com',72,4);
insert into students (id, name, age, email, marks, ranks) values 
(8,'Harish Ram',36,'har@g.com',90,2);
insert into students (id, name, age, email, marks, ranks) values 
(9,'SathyaRaj',38,'sat@g.com',99,1);
insert into students (id, name, age, email, marks, ranks) values 
(10,'Rajaram Mohan',42,'rajmohan@g.com',81,3);
select*from students;
select name,age from students having age>40;
select count(*) from students where age>40;
select marks,count(marks) from students group by marks;
select count(*) from students where age between 47 and 90;
select*from students order by name desc;
select name,marks from students where age=(select max(age) from students);
select*from students where age=60 or age=70;
select distinct ranks from students;
alter table students add column city varchar(20);
