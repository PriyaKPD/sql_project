create schema multi1;

create table multi1.auditor(id int primary key,name varchar(10),age int,email varchar(20),
college_id int,foreign key(college_id) references multi1.college(id));

insert into multi1.auditor values (1001,'priya',23,'pri2@gmailcom',101);
insert into multi1.auditor values (1002,'vidya',22,'vidk@gmailcom',102);
insert into multi1.auditor values (1003,'miya',22,'miya3@gmailcom',102);
insert into multi1.auditor values (1004,'bava',24,'bavas@gmailcom',103);
insert into multi1.auditor values (1005,'swetha',25,'swem@gmailcom',104);
select * from multi1.auditor;

create table multi1.college(id int primary key,name varchar(20),year int,area varchar(20));

insert into multi1.college values (101,'pits',2007,'thanjavur');
insert into multi1.college values (102,'vit',2005,'vellore');
insert into multi1.college values (103,'nit',2009,'chennai');
insert into multi1.college values (104,'fx',2006,'thirunelveli');
select * from multi1.college;

create table multi1.work(id int,auditor_id int,
college_id int,constraint foreign key(auditor_id) references multi1.auditor(id),
constraint foreign key(college_id) references multi1.college(id));
drop table multi1.work;
insert into multi1.work values (1,1001,101);
insert into multi1.work values (2,1002,101);
insert into multi1.work values (3,1002,102);
insert into multi1.work values (4,1001,103);
insert into multi1.work values (5,1001,104);
insert into multi1.work values (6,1003,101);
insert into multi1.work values (7,1004,105);
insert into multi1.work values (8,1005,104);
insert into multi1.work values (9,1005,103);
insert into multi1.work values (10,1004,102);
select* from multi1.work;

