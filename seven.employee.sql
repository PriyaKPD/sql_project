create schema seven;

create table seven.employee(id int PRIMARY KEY,name VARCHAR(20),fname VARCHAR(20),lname VARCHAR(20),age int,email VARCHAR(20));
insert into seven.employee VALUES(101,"ram","ram","kumar",25,"ram@gmail.com"),
(102,"priya","priya","raj",23,"priya@gmail.com"),
 (103,"sita","sita","lakshmi",23,"ram@gmail.com"),
 (104,"raja","raj","kumar",24,"raja@gmail.com"),
(105,"keerthika","keerthi","keerthi",25,"keerthika@gmail.com"),
(106,"ram","ram","kumar",25,"ram@gmail.com"),
(107,"priya","priya","raj",23,"priya@gmail.com"),
 (108,"sita","sita","lakshmi",23,"ram@gmail.com"),
 (109,"raja","raj","kumar",24,"raja@gmail.com"),
(110,"keerthika","keerthi","keerthi",25,"keerthika@gmail.com");
select*from seven.employee;
CREATE table seven.license(licNO  int PRIMARY KEY,rto_area varchar(20),emp_id int,issued_year int );
insert into seven.license VALUES(10001,"thanjavur",102,2020),
(10002,"trichy",101,2022),
(10003,"trichy",103,2021),
(10004,"salem",104,2022),
(10005,"thanjavur",105,2021),
(10006,"thiruvarur",106,2020),
(10007,"chennai",107,2022),
(10008,"salem",108,2021),
(10009,"thanjavur",109,2022),
(10010,"chennai",null,2020);
select * from seven.license;
truncate seven.license;
drop table seven.license;
alter table seven.license add constraint foreign key(emp_id) references seven.employee(id);

select distinct fname from seven.employee;
select count(e.name) from seven.employee e inner join seven.license lic on e.id=lic.emp_id; 
select count(e.name),lic.rto_area from seven.employee e inner join seven.license lic on e.id=lic.emp_id group by lic.rto_area;
select count(e.name),lic.issued_year,lic.rto_area from seven.employee e inner join seven.license lic on e.id=lic.emp_id group by issued_year,rto_area;
select count(e.name) from seven.employee e inner join seven.license lic on e.id=lic.emp_id;
select count(e.name) from seven.employee e inner join seven.license lic on e.id=lic.emp_id;
select count(e.name)>2,lic.rto_area from seven.employee e inner join seven.license lic on e.id=lic.emp_id group by lic.rto_area;