create schema seven1;

create table seven1.employee(id int PRIMARY KEY,name VARCHAR(20),fname VARCHAR(20),lname VARCHAR(20),age int,email VARCHAR(20));
insert into seven1.employee VALUES(101,"ram","ram","kumar",25,"ram@gmail.com"),
(102,"priya","priya","raj",23,"priya@gmail.com"),
 (103,"sita","sita","lakshmi",23,"ram@gmail.com"),
 (104,"raja","raj","kumar",24,"raja@gmail.com"),
(105,"keerthika","keerthi","keerthi",25,"keerthika@gmail.com"),
(106,"ram","ram","kumar",25,"ram@gmail.com"),
(107,"priya","priya","raj",23,"priya@gmail.com"),
 (108,"sita","sita","lakshmi",23,"ram@gmail.com"),
 (109,"raja","raj","kumar",24,"raja@gmail.com"),
(110,"keerthika","keerthi","keerthi",25,"keerthika@gmail.com");
select*from seven1.employee;

create table seven1.project(id int primary key,name varchar(20),employee_id int,foreign key(employee_id) references seven1.employee(id));

insert into seven1.project values (1,'html',101),
(2,'python',102),
(3,'c++',103),
(4,'c',104),
(5,'csharp',105),
(6,'dotnet',null),
(7,'advanced_python',106),
(8,'ruby',107);
select * from seven1.project;

select * from seven1.employee e right outer join seven1.project p on e.id=p.employee_id order by fname;