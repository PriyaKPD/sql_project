create schema ssm;
 
create table ssm.school(id int primary key,name varchar(10),code int);
create table ssm.students(id int primary key,name varchar(20),age int,
email varchar(20),school_id int,foreign key (school_id) references school(id));

insert into ssm.school values (101,'vvs',1010);
insert into ssm.school values (102,'ksms',1011);
insert into ssm.school values (103,'prps',1012);
select * from ssm.school;

insert into ssm.students values (201,'priya',14,'pri2@gmail.com',101);
insert into ssm.students values (202,'vidya',14,'vidya14@gmail.com',101);
insert into ssm.students values (203,'bava',14,'bava17@gmail.com',103);
insert into ssm.students values (204,'sri',14,'sri03@gmail.com',102);
insert into ssm.students values (205,'sara',14,'saraj@gmail.com',102);
select * from ssm.students;
 
 select scl.name as scl_name,students.name as students_name,students.age as age,students.email as email
 from ssm.school scl inner join ssm.students students on scl.id=students.school_id where scl.name='vvs';
 
 create table ssm.mobile(id int primary key,number bigint,operator varchar(20),
 package varchar(25),students_id int,foreign key(students_id)references students(id));

 insert into ssm.mobile values (1001,9345071753,'jio','2gb/day',202),
 (1006,9345071753,'jio','2gb/day',201),
 (1002,9346071753,'vi','1gb/day',203),
 (1003,9345072753,'airtel','3gb/day',204),
 (1004,9345077753,'jio','6gb/day',205),
 (1005,9845071753,'vi','8gb/day',201);
 select * from ssm.mobile;



select students.name as students_name,students.age as age,
 students.email as email,mobile.number,mobile.operator 
from ssm.school school
		inner join ssm.students students 
        inner join ssm.mobile mobile 
        on school.id=students.school_id and students.id=mobile.students_id 
        where school.name='vvs';
