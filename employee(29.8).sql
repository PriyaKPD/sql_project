create schema emp;
use emp;
create table employee(id int,name varchar(20),age int,salary int,organization varchar(25));
insert into employee(id,name,age,salary,organization)values(101,'kalidass',22,70000,'vibro'),
(102,'krisna',23,50000,'tcs'),
(103,'bharath',24,60000,'zogo'),
(104,'padma',23,40000,'vibro'),
(105,'vidya',32,45000,'tcs'),
(106,'bava',25,50000,'zogo'),
(107,'jeeva',26,52000,'vibro'),
(108,'varsha',24,55000,'tcs'),
(109,'sanu',27,64000,'zogo'),
(110,'karthik',25,80000,'vibro');
select*from employee;
select *,salary from employee where salary=(select max(salary)from employee);
select *,age from employee where age=(select min(age)from employee);
select organization,sum(salary) from employee where organization='vibro';
select organization,sum(salary) from employee where organization='tcs';
select organization,sum(salary) from employee where organization='zogo';
select organization,avg(salary) from employee where organization='vibro';
select organization,avg(salary) from employee where organization='tcs';
select organization,avg(salary) from employee where organization='zogo';
select organization,sum(salary) from employee group by organization;
select organization,avg(salary) from employee group by organization;